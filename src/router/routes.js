// IMPORTS OF PAGES

// CREATE ROUTES AND VALIDATIONS
const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: { name: 'LoginForm' }
  },
  {
    path: '/login',
    // IMPORT LAYOUT  ==> DEFAULT
    component: () => import('../components/06-layouts/default.vue'),
    children: [
      {
        path: '',
        name: 'LoginForm',
        component: () => import('../components/05-pages/login/default.vue'),
        meta: {
          guest: true,
          title: 'Ingresar'
        }
      },
      {
        path: '/registro',
        name: 'RegisterPage',
        component: () => import('../components/05-pages/login/register.vue'),
        meta: {
          title: 'Registro'
        }
      }
    ]
  },
  {
    path: '/dashboard',
    // IMPORT LAYOUT  ==> DASHBOARD
    component: () => import('../components/06-layouts/dashboard.vue'),
    children: [
      {
        path: '',
        redirect: { name: 'SessionsPage' },
        meta: { requiresAuth: true }
      },
      {
        path: '/base',
        name: 'BasePage',
        component: () => import('../components/05-pages/base.vue'),
        meta: {
          requiresAuth: true,
          title: 'Base'
        }
      },
      {
        path: '/pacientes',
        name: 'PatientsPage',
        component: () => import('../components/05-pages/patients/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Pacientes'
        }
      },
      {
        path: '/psicologos',
        name: 'PsychologistPage',
        component: () => import('../components/05-pages/psychologist/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Psicologos'
        }
      },
      {
        path: '/protocolos',
        name: 'ProtocolsPage',
        component: () => import('../components/05-pages/protocols/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Protocols'
        }
      },
      {
        path: '/protocolos/detalle/:id',
        name: 'ProtocolsDetailsPage',
        component: () => import('../components/05-pages/protocols/detail.vue'),
        meta: {
          requiresAuth: true,
          title: 'ProtocolsDetails'
        }
      },
      {
        path: '/protocolos/crear',
        name: 'ProtocolsCreatePage',
        component: () => import('../components/05-pages/protocols/create.vue'),
        meta: {
          requiresAuth: true,
          title: 'ProtocolsDetails'
        }
      },
      {
        path: '/sesiones',
        name: 'SessionsPage',
        component: () => import('../components/05-pages/sessions/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Sesiones'
        }
      },
      {
        path: '/sala-de-emociones',
        name: 'EmotionRoomPage',
        component: () => import('../components/05-pages/emotionRooms/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Sala de emociones'
        }
      },
      {
        path: '/sala-de-emociones/crear',
        name: 'EmotionRoomCreatePage',
        component: () => import('../components/05-pages/emotionRooms/create.vue'),
        meta: {
          requiresAuth: true,
          title: 'Nueva sala de emociones'
        }
      },
      {
        path: '/beneficios',
        name: 'BenefitsPage',
        component: () => import('../components/05-pages/benefits/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Beneficios'
        }
      },
      {
        path: '/beneficios/gestion',
        name: 'BenefitsGestionPage',
        component: () => import('../components/05-pages/benefits/gestion.vue'),
        meta: {
          requiresAuth: true,
          title: 'Beneficios Gestion'
        }
      },
      {
        path: '/beneficios/crear',
        name: 'BenefitsCreatePage',
        component: () => import('../components/05-pages/benefits/create.vue'),
        meta: {
          requiresAuth: true,
          title: 'Beneficio editar'
        }
      },
      {
        path: '/beneficios/editar/:id',
        name: 'BenefitsEditPage',
        component: () => import('../components/05-pages/benefits/edit.vue'),
        meta: {
          requiresAuth: true,
          title: 'Beneficio editar'
        }
      },
      {
        path: '/beneficios/:id',
        name: 'BenefitsDetailPage',
        component: () => import('../components/05-pages/benefits/detail.vue'),
        meta: {
          requiresAuth: true,
          title: 'Beneficio'
        }
      },
      {
        path: '/casos',
        name: 'CasePage',
        component: () => import('../components/05-pages/history/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Casos'
        }
      },
      {
        path: '/mottivateca',
        name: 'MottivatecaPage',
        component: () => import('../components/05-pages/mottivateca/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Mottivateca'
        }
      },
      {
        path: '/mottivateca/crear',
        name: 'MottivatecaCreatePage',
        component: () => import('../components/05-pages/mottivateca/create.vue'),
        meta: {
          requiresAuth: true,
          title: 'Mottivateca'
        }
      },
      {
        path: '/mottivateca/:id',
        name: 'MottivatecaDetailPage',
        component: () => import('../components/05-pages/mottivateca/detail.vue'),
        meta: {
          requiresAuth: true,
          title: 'Mottivateca'
        }
      },
      {
        path: '/creador',
        name: 'CreatorPage',
        component: () => import('../components/05-pages/creator/user.vue'),
        meta: {
          requiresAuth: true,
          title: 'Creador'
        }
      },
      {
        path: '/creador/institucion',
        name: 'CreatorInstitutionPage',
        component: () => import('../components/05-pages/institution/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Creador Institucion'
        }
      },
      {
        path: '/creador/subir',
        name: 'CreatorSavePage',
        component: () => import('../components/05-pages/creator/files.vue'),
        meta: {
          requiresAuth: true,
          title: 'Creador Subir'
        }
      },
      {
        path: '/creador/listar',
        name: 'CreatorListPage',
        component: () => import('../components/05-pages/creator/table.vue'),
        meta: {
          requiresAuth: true,
          title: 'Creador Listado'
        }
      },
      {
        path: '/creador/listar-erroneos',
        name: 'CreatorListErrorPage',
        component: () => import('../components/05-pages/creator/table-error.vue'),
        meta: {
          requiresAuth: true,
          title: 'Creador Listado Erroneos'
        }
      },
      {
        path: '/estadisticas',
        name: 'StadisticsPage',
        component: () => import('../components/05-pages/statistics/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Estadisticas'
        }
      },
      {
        path: '/gubernamental',
        name: 'GubernamentalPage',
        component: () => import('../components/05-pages/gubernamental/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Gubernamental'
        }
      },
      {
        path: '/gubernamental/:id',
        name: 'GubernamentalPatientPage',
        component: () => import('../components/05-pages/gubernamental/patients.vue'),
        meta: {
          requiresAuth: true,
          title: 'Gubernamental Pacientes'
        }
      },
      {
        path: '/gestores',
        name: 'ManagersPage',
        component: () => import('../components/05-pages/managers/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Gestores'
        }
      },
      {
        path: '/super-ma',
        name: 'SuperMaPage',
        component: () => import('../components/05-pages/superMa/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Gestores'
        }
      },
      {
        path: '/mensajes',
        name: 'MessagesPage',
        component: () => import('../components/05-pages/messages/default.vue'),
        meta: {
          requiresAuth: false,
          title: 'Mensajes'
        }
      },
      {
        path: '/index',
        name: 'IndexPage',
        component: () => import('../components/05-pages/index/default.vue'),
        meta: {
          requiresAuth: false,
          title: 'Inicio'
        }
      },
      {
        path: '/perfil',
        name: 'ProfilePage',
        component: () => import('../components/05-pages/profile/default.vue'),
        meta: {
          requiresAuth: false,
          title: 'Perfil'
        }
      }
    ]
  }
]

export default routes
