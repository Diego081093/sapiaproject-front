// IMPORT LIBRARIES
import Vue from 'vue'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_ROOMS: [],
  S_PARTICIPANTS: {},
  S_ROOM: {},
  S_CHAT: {}
}
const getters = {
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  }
}
const actions = {
  async A_GET_ROOMS ({ commit }, Params) {
    try {
      const { data } = await HTTP.get(`emotions-room?to=${Params.to}`)

      commit('SET_DATA', {
        destination: 'S_ROOMS',
        data: data.rooms
      })
      return data
    } catch (error) {
      console.log('ERROR_ROOMS [ACTION]', error)
      throw error
    }
  },
  async A_GET_ROOM ({ commit }, Params) {
    try {
      const { data } = await HTTP.get(`emotions-room/${Params.id}`)

      commit('SET_DATA', {
        destination: 'S_ROOM',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_ROOM [ACTION]', error)
    }
  },
  async A_CLOSE_ROOM ({ commit }, params) {
    try {
      const { data } = await HTTP.put('room', params)
      return data
    } catch (error) {
      console.log('ERROR_ROOM [ACTION]', error)
    }
  },
  async A_GET_PARTICIPANTS_ROOM ({ commit }, Params) {
    try {
      const { data } = await HTTP.get(`emotions-room/participant/${Params.id}`)

      commit('SET_DATA', {
        destination: 'S_PARTICIPANTS',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_ROOM [ACTION]', error)
    }
  },
  async A_SAVE_NOTE ({ commit }, params) {
    try {
      const { data } = await HTTP.post('room/' + params.idRoom + '/notes', {
        note: params.note,
        priority: params.priority
      })
      commit('SET_DATA', {
        destination: 'S_NOTE',
        data: data
      })
    } catch (error) {
      console.log('ERROR_SAVE_NOTE [ACTION]', error)
      throw error
    }
  },
  async A_GET_ROOM_CHAT ({ commit }, id) {
    try {
      const { data } = await HTTP.get(`chat/${id}`)
      commit('SET_DATA', {
        destination: 'S_CHAT',
        data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_SESSION:', error)
      throw error
    }
  },
  async A_GET_ROOM_PRIVATE_CHAT ({ commit }, id) {
    try {
      const { data } = await HTTP.get(`chat/${id}`)
      return data.data
    } catch (error) {
      console.log('ERROR_GET_SESSION:', error)
      throw error
    }
  },
  async A_SET_ROOM_MESSAGE ({ commit }, body) {
    try {
      const { data } = await HTTP.post('chat/message', body)
      return data
    } catch (error) {
      console.log('Something a went error:', error)
      throw error
    }
  },
  async A_GET_PRIVATE_CHAT ({ commit }, params) {
    try {
      const { data } = await HTTP.get('chat/private?idRoom=' + params.idRoom + '&idPatient=' + params.idPatient)
      return data.data
    } catch (error) {
      console.log('Something a went error:', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
