import Vue from 'vue'
import { HTTP } from '@/plugins/axios'

const state = {
  S_BLOCKEDS: []
}
const getters = {}
const mutations = {
  SET_DATA (state, params) {
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  }
}
const actions = {
  async A_GET_BLOCKEDS ({ commit }) {
    try {
      const { data } = await HTTP.get('sessionCases')
      commit('SET_DATA', {
        destination: 'S_BLOCKEDS',
        data: data.data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_BLOCKEDS [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
