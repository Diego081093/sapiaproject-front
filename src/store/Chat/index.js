// IMPORT LIBRARIES
import Vue from 'vue'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_CHATS: null,
  S_CHAT: null,
  S_MESSAGE: null
}
const getters = {
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  }
}
const actions = {
  async A_GET_CHATS ({ commit }, reference) {
    try {
      const { data } = await HTTP.get('chat/helpdesk/list?reference=' + reference)

      commit('SET_DATA', {
        destination: 'S_CHATS',
        data: data.data
      })
      return data.data
    } catch (error) {
      console.log('ERROR_GET_CHATS [ACTION]', error)
      throw error
    }
  },
  async A_JOIN_CHAT ({ commit }, id) {
    try {
      const { data } = await HTTP.post('chat/helpdesk', { id })
      commit('SET_DATA', {
        destination: 'S_CHAT',
        data: data.data
      })
      return data.data
    } catch (error) {
      console.log('ERROR_GET_CHATS [ACTION]', error)
      throw error
    }
  },
  async A_GET_CHAT ({ commit }, id) {
    try {
      const { data } = await HTTP.get('chat/' + id)
      commit('SET_DATA', {
        destination: 'S_CHAT',
        data: data.data
      })
      return data.data
    } catch (error) {
      console.log('ERROR_GET_CHATS [ACTION]', error)
      throw error
    }
  },
  async A_POST_MESSAGE ({ commit }, params) {
    try {
      const { data } = await HTTP.post('chat/message', { id: params.id, message: params.message })
      commit('SET_DATA', {
        destination: 'S_MESSAGE',
        data: data.data
      })
      return data.data
    } catch (error) {
      console.log('ERROR_GET_CHATS [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
