import Vue from 'vue'
import { HTTP } from '@/plugins/axios'

const state = {
  S_BENEFITS: [],
  S_BENEFITS_GESTION: []
}
const getters = {}
const mutations = {
  SET_DATA (state, params) {
    Vue.set(state, params.destination, params.data)
  }
}
const actions = {
  async A_GET_BENEFITS ({ commit }) {
    try {
      const { data } = await HTTP.get('benefits')
      commit('SET_DATA', {
        destination: 'S_BENEFITS',
        data: data.data.benefits
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_BENEFITS:', error)
      throw error
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
