// IMPORT LIBRARIES
import Vue from 'vue'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_INSTITUTIONS: []
}
const getters = {
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  }
}
const actions = {
  async A_GET_INSTITUTIONS ({ commit }, params) {
    try {
      const { data } = await HTTP.get('institution', { params })
      commit('SET_DATA', {
        destination: 'S_INSTITUTIONS',
        data: data.data
      })
      return data.data
    } catch (error) {
      console.log('ERROR_GET_INSTITUTION [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
