// IMPORT LIBRARIES
import Vue from 'vue'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_METHODS: []
}
const getters = {
  G_METHODS: (state) => {
    return state.S_METHODS.map(element => ({
      name: element.name,
      value: String(element.id)
    }))
  }
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  }
}
const actions = {
  async A_GET_METHODS ({ commit }, params) {
    try {
      const { data } = await HTTP.get('method', { params })
      commit('SET_DATA', {
        destination: 'S_METHODS',
        data: data.data
      })
      return data.data
    } catch (error) {
      console.log('ERROR_GET_METHODS [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
