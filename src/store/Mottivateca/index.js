import Vue from 'vue'
import { HTTP } from '@/plugins/axios'

const state = {
  S_MOTTIVATECAS: [],
  S_TRAINING: {}
}
const getters = {}
const mutations = {
  SET_DATA (state, params) {
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  }
}
const actions = {
  async A_GET_MOTTIVATECAS ({ commit }, params) {
    try {
      const { data } = await HTTP.get('media', { params })
      commit('SET_DATA', {
        destination: 'S_MOTTIVATECAS',
        data: data.data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_MOTTIVATECAS [ACTION]', error)
      throw error
    }
  },
  async A_GET_TRAINING ({ commit }, id) {
    try {
      const { data } = await HTTP.get(`trainings/${id}`)

      commit('SET_DATA', {
        destination: 'S_TRAINING',
        data: data.data
      })
      return data
    } catch (error) {
      console.log('ERROR_LOGIN [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
