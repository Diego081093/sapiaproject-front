// IMPORT LIBRARIES
import Vue from 'vue'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_ROOMS: []
}
const getters = {
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  }
}
const actions = {
  async A_GET_ROOMS ({ commit }, params) {
    try {
      const { data } = await HTTP.get('room', { params })

      commit('SET_DATA', {
        destination: 'S_ROOMS',
        data: data.data
      })
      return data.data
    } catch (error) {
      console.log('ERROR_LOGIN [ACTION]', error)
      throw error
    }
  },
  async A_SET_ROOM ({ commit }, body) {
    try {
      const { data } = await HTTP.post('room', body)

      commit('PUSH_DATA', {
        destination: 'S_ROOMS',
        data: body
      })
      return data
    } catch (error) {
      console.log('ERROR_SET_ROOMS', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
