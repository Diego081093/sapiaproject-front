// IMPORT LIBRARIES
import Vue from 'vue'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_DIARIES: null,
  S_DIARY: {}
}
const getters = {
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  }
}
const actions = {
  async A_GET_DIARIES ({ commit }, params) {
    try {
      const { data } = await HTTP.get('diary/viewPsychologist', { params })
      commit('SET_DATA', {
        destination: 'S_DIARIES',
        data: data.data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_DIARIES:', error)
      throw error
    }
  },
  async A_SET_SEEN ({ commit }, params) {
    try {
      const { data } = await HTTP.put('diary/seen/' + params.id, { params })
      commit('SET_DATA', {
        destination: 'S_DIARY',
        data: data.data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_DIARIES:', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
